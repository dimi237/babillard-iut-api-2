import { Request, Response } from 'express';
import { usersService } from '../Services/userService';

export const usersController = {
    init: (app: any): void => {

        app.get('/users/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data: any = await usersService.getUserById(id);
             res.status(200).json(data);
        });

        app.get('/users', async (req: Request, res: Response): Promise<any> => {
            const data: any = await usersService.getUsers();
            res.status(200).json(data);
        });
        app.post('/users', async (req: Request, res: Response): Promise<any> => {
            const data = await usersService.insertUser(req.body);

            res.status(200).json(data);
        });
        app.put('/users/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await usersService.updateUser(id, req.body);

            res.status(200).json(data);
    });
        app.delete('/users/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await usersService.deleteUser(id);


            res.status(200).json({ data});
        });

        app.post('/login', async (req: Request, res: Response): Promise<any> => {
            const data = await usersService.authService(req.body);
             res.status(200).json(data);
        });
    }
}