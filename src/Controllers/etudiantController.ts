import { Request, Response } from 'express';
import { etudiantService } from '../Services/etudiantService';

export const etudiantController = {
    init: (app: any): void => {

        app.get('/etudiant/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data: any = await etudiantService.getEtudiantById(id);
             res.status(200).json(data);
        });

        app.get('/etudiant', async (req: Request, res: Response): Promise<any> => {
            const data: any = await etudiantService.getEtudiant();
            res.status(200).json(data);
        });
        app.get('/etudiants', async (req: Request, res: Response): Promise<any> => {
            const data: any = await etudiantService.getEtudiantBy(req.query);
            res.status(200).json(data);
        });
        app.post('/etudiant', async (req: Request, res: Response): Promise<any> => {
            const data = await etudiantService.insertEtudiant(req.body);

            res.status(200).json(data);
        });
        app.put('/etudiant/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await etudiantService.updateEtudiant(id, req.body);

            res.status(200).json(data);
    });
        app.delete('/etudiant/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await etudiantService.deleteEtudiant(id);


            res.status(200).json({ data});
        });

    }
}