import { Request, Response } from 'express';
import { profService } from '../Services/profService';

export const profController = {
    init: (app: any): void => {

        app.get('/prof/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data: any = await profService.getProfById(id);
             res.status(200).json(data);
        });

        app.get('/prof', async (req: Request, res: Response): Promise<any> => {
            const data: any = await profService.getProf();
            res.status(200).json(data);
        });
        app.post('/prof', async (req: Request, res: Response): Promise<any> => {
            const data = await profService.insertProf(req.body);

            res.status(200).json(data);
        });
        app.put('/prof/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await profService.updateProf(id, req.body);

            res.status(200).json(data);
    });
        app.delete('/prof/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await profService.deleteProf(id);


            res.status(200).json({ data});
        });

      
    }
}