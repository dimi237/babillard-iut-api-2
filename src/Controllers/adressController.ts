import { Request, Response } from 'express';
import { adressService } from '../Services/adresseService';

export const adressController = {
    init: (app: any): void => {

        app.get('/adress/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data: any = await adressService.getAdressById(id);
             res.status(200).json(data);
        });

        app.get('/adress', async (req: Request, res: Response): Promise<any> => {
            const data: any = await adressService.getAdresses();
            res.status(200).json(data);
        });
        app.post('/adress', async (req: Request, res: Response): Promise<any> => {
            const data = await adressService.insertAdress(req.body);

            res.status(200).json(data);
        });
        app.put('/adress/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await adressService.updateAdress(id, req.body);

            res.status(200).json(data);
    });
        app.delete('/adress/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await adressService.deleteAdress(id);


            res.status(200).json({ data});
        });
    }
}