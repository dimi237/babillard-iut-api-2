import { Request, Response } from 'express';
import { noteService } from '../Services/noteService';

export const noteController = {
    init: (app: any): void => {

        app.get('/note/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data: any = await noteService.getNoteById(id);
             res.status(200).json(data);
        });

        app.get('/note', async (req: Request, res: Response): Promise<any> => {
            const data: any = await noteService.getNotes();
            res.status(200).json(data);
        });
        app.post('/note', async (req: Request, res: Response): Promise<any> => {
            const data = await noteService.insertNote(req.body);

            res.status(200).json(data);
        });
        app.put('/note/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await noteService.updateNote(id, req.body);

            res.status(200).json(data);
    });
        app.delete('/note/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await noteService.deleteNote(id);


            res.status(200).json({ data});
        });

    }
}