import { Request, Response } from 'express';
import { matiereService } from '../Services/matiereService';

export const matiereController = {
    init: (app: any): void => {

        app.get('/matiere/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data: any = await matiereService.getMatiereById(id);
             res.status(200).json(data);
        });

        app.get('/matiere', async (req: Request, res: Response): Promise<any> => {
            const data: any = await matiereService.getMatieres();
            res.status(200).json(data);

            if (data instanceof Error && data === null ) {
                return res.status(400).json('problem');
            }
        });
        app.get('/matieres', async (req: Request, res: Response): Promise<any> => {

            
            const data: any = await matiereService.getMatiereBy(req.query);
            res.status(200).json(data);

            if (data instanceof Error && data === null ) {
                return res.status(400).json('problem');
            }
        });
        app.post('/matiere', async (req: Request, res: Response): Promise<any> => {
            const data = await matiereService.insertMatiere(req.body);

            res.status(200).json(data);
        });
        app.put('/matiere/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await matiereService.updateMatiere(id, req.body);

            res.status(200).json(data);
    });
        app.delete('/matiere/:id', async (req: Request, res: Response): Promise<any> => {
            const { id } = req.params;
            const data = await matiereService.deleteMatiere(id);


            res.status(200).json({ data});
        });
    }
}