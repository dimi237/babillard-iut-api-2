import {  getDb } from './config';
import { ObjectID } from 'mongodb';
import { Matiere } from '../models/matiere';

const collectionName = 'matiere';

export const matiereCollection = {

    getMatiereByQuery: async (data:any) : Promise<Matiere[]>=> {
        const database = await getDb();        
        return database.collection(collectionName).find(data).toArray();
        
    },

    getMatiere: async (): Promise<Matiere[]> => {
        // await connect()
        const database = await getDb();
        const matiere = await database.collection(collectionName).find().toArray();
        return matiere;
    },

    getMatiereById: async (id: any): Promise<Matiere> => {
        const database = await getDb();
        return await database.collection(collectionName).findOne({ "_id": new ObjectID(id) });
    },

    insertMatiere: async (matiere: Matiere) => {
        const database = await getDb();
        const { insertedId } = await database.collection(collectionName).insertOne(matiere);
        return insertedId;
    },

    updateMatiere: async (id: string, set: Matiere) => {
        const database = await getDb();

        return await database.collection(collectionName).updateOne({ _id: new ObjectID(id) }, { $set: set });

    },
    deleteMatiere: async (id: string) => {
        const database = await getDb();
        return await database.collection(collectionName).deleteOne({ _id: new ObjectID(id) });
    },

}