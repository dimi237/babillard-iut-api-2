import {  getDb } from './config';
import { ObjectID } from 'mongodb';
import { Prof } from '../models/prof';

const collectionName = 'prof';

export const profCollection = {

    getProfByQuery: async (data:any) : Promise<Prof>=> {
        const database = await getDb();
        const prof = database.collection(collectionName).findOne(data);
        return prof;
    },

    getProf: async (): Promise<Prof[]> => {
        // await connect()
        const database = await getDb();
        const prof = await database.collection(collectionName).find().toArray();
        return prof;
    },

    getProfById: async (id: any): Promise<Prof> => {
        const database = await getDb();
        return await database.collection(collectionName).findOne({ "_id": new ObjectID(id) });
    },

    insertProf: async (prof: Prof) => {
        const database = await getDb();
        const { insertedId } = await database.collection(collectionName).insertOne(prof);
        return insertedId;
    },

    updateProf: async (id: string, set: Prof) => {
        const database = await getDb();

        return await database.collection(collectionName).updateOne({ _id: new ObjectID(id) }, { $set: set });

    },
    deleteProf: async (id: string) => {
        const database = await getDb();
        return await database.collection(collectionName).deleteOne({ _id: new ObjectID(id) });
    },

}