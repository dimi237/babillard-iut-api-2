import {  getDb } from './config';
import { ObjectID } from 'mongodb';
import { Etudiant } from '../models/etudiant';

const collectionName = 'etudiant';

export const etudiantCollection = {

    getEtudiantByQuery: async (data:any) : Promise<Etudiant[]>=> {
        const database = await getDb();
        const etudiant = database.collection(collectionName).find(data).toArray();
        return etudiant;
    },

    getEtudiant: async (): Promise<Etudiant[]> => {
        // await connect()
        const database = await getDb();
        const etudiant = await database.collection(collectionName).find().toArray();
        return etudiant;
    },

    getetudiantById: async (id: any): Promise<Etudiant> => {
        const database = await getDb();
        return await database.collection(collectionName).findOne({ "_id": new ObjectID(id) });
    },

    insertEtudiant: async (etudiant: Etudiant) => {
        const database = await getDb();
        const { insertedId } = await database.collection(collectionName).insertOne(etudiant);
        return insertedId;
    },

    updateEtudiant: async (id: string, set: Etudiant) => {
        const database = await getDb();

        return await database.collection(collectionName).updateOne({ _id: new ObjectID(id) }, { $set: set });

    },
    deleteEtudiant: async (id: string) => {
        const database = await getDb();
        return await database.collection(collectionName).deleteOne({ _id: new ObjectID(id) });
    },

}