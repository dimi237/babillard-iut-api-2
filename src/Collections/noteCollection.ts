import {  getDb } from './config';
import { ObjectID } from 'mongodb';
import { Note } from '../models/note';

const collectionName = 'note';

export const noteCollection = {

    getNoteByQuery: async (data:any) : Promise<Note>=> {
        const database = await getDb();
        const note = database.collection(collectionName).findOne(data);
        return note;
    },

    getNote: async (): Promise<Note[]> => {
        // await connect()
        const database = await getDb();
        const note = await database.collection(collectionName).find().toArray();
        return note;
    },

    getNoteById: async (id: any): Promise<Note> => {
        const database = await getDb();
        return await database.collection(collectionName).findOne({ "_id": new ObjectID(id) });
    },

    insertNote: async (note: Note) => {
        const database = await getDb();
        const { insertedId } = await database.collection(collectionName).insertOne(note);
        return insertedId;
    },

    updateNote: async (id: string, set: Note) => {
        const database = await getDb();

        return await database.collection(collectionName).updateOne({ _id: new ObjectID(id) }, { $set: set });

    },
    deleteNote: async (id: string) => {
        const database = await getDb();
        return await database.collection(collectionName).deleteOne({ _id: new ObjectID(id) });
    },

}