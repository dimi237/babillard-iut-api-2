import {  getDb } from './config';
import { ObjectID } from 'mongodb';
import { Adress } from '../models/adress';

const collectionName = 'adresses';

export const adressCollection = {

    getAdressByQuery: async (data:any) : Promise<Adress>=> {
        const database = await getDb();
        const adress = database.collection(collectionName).findOne(data);
        return adress;
    },

    getAdresses: async (): Promise<Adress[]> => {
        // await connect()
        const database = await getDb();
        const adress = await database.collection(collectionName).find().toArray();
        return adress;
    },

    getAdressById: async (id: any): Promise<Adress> => {
        const database = await getDb();
        return await database.collection(collectionName).findOne({ "_id": new ObjectID(id) });
    },

    insertAdress: async (adress: Adress) => {
        const database = await getDb();
        const { insertedId } = await database.collection(collectionName).insertOne(adress);
        return insertedId;
    },

    updateAdress: async (id: string, set: Adress) => {
        const database = await getDb();

        return await database.collection(collectionName).updateOne({ _id: new ObjectID(id) }, { $set: set });

    },
    deleteAdress: async (id: string) => {
        const database = await getDb();
        return await database.collection(collectionName).deleteOne({ _id: new ObjectID(id) });
    },

}