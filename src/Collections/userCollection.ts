
import { getDb } from './config';
import { ObjectID } from 'mongodb';
import { User } from '../models/user';

const collectionName = 'users';



export const usersCollection = {


    getUserByQuery: async (data: any): Promise<User> => {
        const database = await getDb();
        const user = await database.collection(collectionName).findOne(data);
        return user;
    },

    getUsers: async (): Promise<User[]> => {
        // await connect()
        const database = await getDb();
        const users = await database.collection(collectionName).find().toArray();
        return users;
    },

    getUserById: async (id: any): Promise<User> => {
        const database = await getDb();
        return await database.collection(collectionName).findOne({ "_id": new ObjectID(id) });
    },

    insertUser: async (user: User) => {
        const database = await getDb();
        const { insertedId } = await database.collection(collectionName).insertOne(user);
        return insertedId;
    },

    updateUser: async (id: string, set: User) => {
        const database = await getDb();

        return await database.collection(collectionName).updateOne({ _id: new ObjectID(id) }, { $set: set });

    },
    deleteUser: async (id: string) => {
        const database = await getDb();
        return await database.collection(collectionName).deleteOne({ _id: new ObjectID(id) });
    },
}