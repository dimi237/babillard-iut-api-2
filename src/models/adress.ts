export interface Adress {
    name:  string,
    surname: string,
    email: string,
    postalCode: string,
    phone: string
}