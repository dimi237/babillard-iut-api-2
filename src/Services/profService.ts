import { profCollection } from "../Collections/profCollection";
import jwt from 'jsonwebtoken';
import { Prof } from "../models/prof";
import { config } from "../config";
import { logger } from "../winston";


export const profService = {

    getProfById: async (id: string): Promise<Prof> => {
        try {
            return await profCollection.getProfById(id);
        } catch (error : any) {
            logger.error(`failed to get prof by id \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    getProf: async (): Promise<Prof[]> => {
        try {
            return await profCollection.getProf();
        } catch (error : any) {
            logger.error(`failed to get prof \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    insertProf: async (prof: Prof): Promise<any> => {
        try {
            const result = await profCollection.insertProf(prof);
            return { id: result };
        } catch (error : any) {
            logger.error(`failed to insrt prof \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    updateProf: async (id: string, prof: Prof): Promise<any> => {

        try {
            return await profCollection.updateProf(id, prof);
        } catch (error : any) {
            logger.error(`failed to update prof\n${error.message}\n${error.stack}`);
            return error;
        }
    },



    deleteProf: async (id: string): Promise<any> => {

        try {
            return await profCollection.deleteProf(id);
        } catch (error : any) {
            logger.error(`failed to delete prof \n${error.message}\n${error.stack}`);
            return error;
        }
    },



}