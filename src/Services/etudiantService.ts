import { etudiantCollection } from "../Collections/etudiantCollection";
import jwt from 'jsonwebtoken';
import { Etudiant } from "../models/etudiant";
import { config } from "../config";
import { logger } from "../winston";


export const etudiantService = {

    getEtudiantById: async (id: string): Promise<any> => {
        try {
            return await etudiantCollection.getetudiantById(id);
        } catch (error : any) {
            logger.error(`failed to get etudiant by id `);
            return error ;
              
        }
    },
    getEtudiantBy: async (data : any): Promise<any[]> => {
        try {
            return await etudiantCollection.getEtudiantByQuery(data);
        } catch (error : any) {
            logger.error(`failed to get etudiant by id `);
            return error ;
              
        }
    },
    

    getEtudiant: async (): Promise<any[]> => {
        try {
            return await etudiantCollection.getEtudiant();
        } catch (error : any) {
            logger.error(`failed to get etudiant `);
            return [error , ''];
        }
    },

    insertEtudiant: async (etudiant: Etudiant): Promise<any> => {
        try {
            const result = await etudiantCollection.insertEtudiant(etudiant);
            return { id: result };
        } catch (error : any) {
            logger.error(`failed to insrt etudiant `);
        }
    },

    updateEtudiant: async (id: string, etudiant: Etudiant): Promise<any> => {

        try {
            return await etudiantCollection.updateEtudiant(id, etudiant);
        } catch (error : any) {
            logger.error(`failed to update etudiant`);
        }
    },



    deleteEtudiant: async (id: string): Promise<any> => {

        try {
            return await etudiantCollection.deleteEtudiant(id);
        } catch (error : any) {
            logger.error(`failed to delete etudiant `);
        }
    },

  

}