import { noteCollection } from "../Collections/noteCollection";
import jwt from 'jsonwebtoken';
import { Note } from "../models/note";
import { config } from "../config";
import { logger } from "../winston";


export const noteService = {

    getNoteById: async (id: string): Promise<Note> => {
        try {
            return await noteCollection.getNoteById(id);
        } catch (error : any) {
            logger.error(`failed to get note by id \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    getNotes: async (): Promise<Note[]> => {
        try {
            return await noteCollection.getNote();
        } catch (error : any) {
            logger.error(`failed to get note \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    insertNote: async (note: Note): Promise<any> => {
        try {
            const result = await noteCollection.insertNote(note);
            return { id: result };
        } catch (error : any) {
            logger.error(`failed to insrt note \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    updateNote: async (id: string, note: Note): Promise<any> => {

        try {
            return await noteCollection.updateNote(id, note);
        } catch (error : any) {
            logger.error(`failed to update note\n${error.message}\n${error.stack}`);
            return error;
        }
    },



    deleteNote: async (id: string): Promise<any> => {

        try {
            return await noteCollection.deleteNote(id);
        } catch (error : any) {
            logger.error(`failed to delete note \n${error.message}\n${error.stack}`);
            return error;
        }
    }

 

}