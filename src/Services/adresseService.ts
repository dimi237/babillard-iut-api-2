import { adressCollection } from "../Collections/adressCollection";
import jwt from 'jsonwebtoken' ;
import { Adress } from "../models/adress";
import { logger } from "../winston";


export const adressService = {

    getAdressById: async (id: string): Promise<Adress> => {
        try {
            return  await adressCollection.getAdressById(id);
        } catch (error : any) {
            logger.error(`failed to get adress by id \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    getAdresses: async (): Promise<Adress[]> => {
        try {
            return await adressCollection.getAdresses();
        } catch (error : any) {
            logger.error(`failed to get adresses \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    insertAdress: async (adress: Adress): Promise<any> => {
        try {
            const result = await adressCollection.insertAdress(adress);
            return { id: result };
        } catch (error : any) {
            logger.error(`failed to insert adress \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    updateAdress: async (id: string, adress: Adress): Promise<any> => {

        try {
            return await adressCollection.updateAdress(id, adress);
        } catch (error : any) {
            logger.error(`failed to update adress \n${error.message}\n${error.stack}`);
            return error;
        }
    },



    deleteAdress: async (id: string): Promise<any> => {

        try {
            return await adressCollection.deleteAdress(id);
        } catch (error : any) {
            return error;
        }
    },

   }