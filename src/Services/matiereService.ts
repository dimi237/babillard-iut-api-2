import { matiereCollection } from "../Collections/matiereCollection";
import jwt from 'jsonwebtoken';
import { Matiere } from "../models/matiere";
import { config } from "../config";
import { logger } from "../winston";


export const matiereService = {

    getMatiereById: async (id: string): Promise<Matiere> => {
        try {
            return await matiereCollection.getMatiereById(id);
        } catch (error : any) {
            logger.error(`failed to get matiere by id \n${error.message}\n${error.stack}`);
            return error;
        }
    },
    getMatiereBy: async (param: any): Promise<Matiere[]> => {
        try {
            return await matiereCollection.getMatiereByQuery(param);
        } catch (error : any) {
            logger.error(`failed to get matiere by id \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    getMatieres: async (): Promise<Matiere[]> => {
        try {
            return await matiereCollection.getMatiere();
        } catch (error : any) {
            logger.error(`failed to get matieres \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    insertMatiere: async (matiere: Matiere): Promise<any> => {
        try {
            const result = await matiereCollection.insertMatiere(matiere);
            return { id: result };
        } catch (error : any) {
            logger.error(`failed to insrt matiere \n${error.message}\n${error.stack}`);
            return error;
        }
    },

    updateMatiere: async (id: string, matiere: Matiere): Promise<any> => {

        try {
            return await matiereCollection.updateMatiere(id, matiere);
        } catch (error : any) {
            logger.error(`failed to update matiere\n${error.message}\n${error.stack}`);
            return error;
        }
    },



    deleteMatiere: async (id: string): Promise<any> => {

        try {
            return await matiereCollection.deleteMatiere(id);
        } catch (error : any) {
            logger.error(`failed to delete matiere \n${error.message}\n${error.stack}`);
            return error;
        }
    },


}