import { profController } from './Controllers/profController';
import { noteController } from './Controllers/noteController';
import { matiereCollection } from './Collections/matiereCollection';
import express from 'express';
import { json, urlencoded } from 'body-parser';
import { config } from './config';
import  helmet from 'helmet';
import cors from 'cors';
import morgan from 'morgan';
import { logger, morganOption } from './winston';
import  xmlparser from 'express-xml-bodyparser';
import  httpContext from 'express-http-context';

import { oauthVerification } from './middlewares/auth.middleware';
import { adressController } from './Controllers/adressController';
import { usersController } from './Controllers/userController';
import { etudiantController } from './Controllers/etudiantController';
import { matiereController } from './Controllers/matiereController';

const Helmet = helmet as any;
const app = express();
app.use(Helmet());

// using bodyParser
app.use(urlencoded({ extended: true }));

// using bodyParser to parse JSON bodies into JS objects
app.use(json({ limit: '10mb' }));

// using XML body parser
app.use(xmlparser());

// enabling CORS for all requests
app.use(cors({ origin: true, credentials: true }));

// adding morgan to log HTTP requests
const format = ':remote-addr - ":method :url HTTP/:http-version" :status :response-time ms - :res[content-length] ":referrer" ":user-agent"';
app.use(morgan(format, morganOption));

// Apply middlewares
app.use(httpContext.middleware);
/* app.use(oauthVerification); */

// Definition controllers
/* adressController.init(app);
usersController.init(app); */
etudiantController.init(app);
matiereController.init(app);
noteController.init(app);
profController.init(app);



const main = express().use(config.get('basePath') || '', app);

main.listen(config.get('port'), async () => {
    logger.info(`server started. Listening on port ${config.get('port')} in "${config.get('env')}" mode`);
});
/* cronService.startCronTransfer(); */
export default app;


